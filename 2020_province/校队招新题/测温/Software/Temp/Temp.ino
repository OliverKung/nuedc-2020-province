#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <splash.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for SSD1306 display connected using software SPI (default case):
#define OLED_MOSI  5
#define OLED_CLK   6
#define OLED_DC    7
#define OLED_CS    8
#define OLED_RESET 9
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT,
  OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

void displayGUI(double ADC_Value)
{
  display.clearDisplay();
  display.setTextSize(2);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.write('T');
  display.write('e');
  display.write('m');
  display.write('p');
  display.write(':');
  int Buffer=ADC_Value;
  display.write(Buffer/1000+48);
  display.write((Buffer/100)%10+48);
  display.write((Buffer/10)%10+48);
  display.write('.');
  display.write((Buffer)%10+48);
  display.display();
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(A3,INPUT);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.display();
  delay(2000); // Pause for 2 seconds
}

void loop() {
  // put your main code here, to run repeatedly:
  double ADC_Value=0;
  for(int i=0;i<20;i++)
  {
    ADC_Value+=analogRead(A3);
  }
  ADC_Value=ADC_Value/20;
  ADC_Value=0.0238*ADC_Value*ADC_Value-30.546*ADC_Value+9815.6;
  displayGUI(ADC_Value);
}
