module top(
	input clk,
	output PWM,nPWM
);

reg [9:0]FreqControl,DutyControl;

initial
begin
FreqControl=10'b0000000000;
DutyControl=512;
end

always@(posedge clk)
begin
	if(DutyControl>922)
	begin
		DutyControl=102;
	end
	else
	begin
		DutyControl=DutyControl+1;
	end
end
wire clk_400MHz;
ClockSource uClock(
	.refclk(clk),
	.reset(0),
	.clk0_out(clk_400MHz)
);
uPWMGenerator PWMGeneratorPhase1(
	.clk(clk_400MHz),
	.reset(0),
	.DutyControl(DutyControl),
	.FreqControl(FreqControl),
	.PWM(PWM),
	.nPWM(nPWM)
);

endmodule
