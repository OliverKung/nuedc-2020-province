module uPWMGenerator( 
	input clk,
	input reset,
	input [9:0]DutyControl,FreqControl,
	output PWM,nPWM
);
reg [9:0]counter=10'b0000000000;
initial
begin
	counter=10'b0000000000;
end

always@(posedge clk)
begin
	counter=counter+1;
end

assign PWM=counter>DutyControl?1:0;

assign nPWM=~PWM;

endmodule
