<?xml version="1.0" encoding="UTF-8"?>
<Project>
    <Project_Created_Time>2020-09-25 19:07:26</Project_Created_Time>
    <TD_Version>4.6.13941</TD_Version>
    <UCode>00000000</UCode>
    <Name>PWM_Generator</Name>
    <HardWare>
        <Family>EG4</Family>
        <Device>EG4S20BG256</Device>
    </HardWare>
    <Source_Files>
        <Verilog>
            <File>top.v</File>
            <File>uPWMGenerator.v</File>
            <File>al_ip/ClockSource.v</File>
        </Verilog>
        <ADC_FILE>io.adc</ADC_FILE>
        <SDC_FILE/>
        <CWC_FILE/>
    </Source_Files>
    <TOP_MODULE>
        <LABEL/>
        <MODULE>top</MODULE>
        <CREATEINDEX>user</CREATEINDEX>
    </TOP_MODULE>
    <Project_Settings>
        <Step_Last_Change>2020-09-25 21:28:46</Step_Last_Change>
        <Current_Step>60</Current_Step>
        <Step_Status>true</Step_Status>
    </Project_Settings>
</Project>
